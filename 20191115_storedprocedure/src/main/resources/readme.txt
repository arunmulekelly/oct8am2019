DROP PROCEDURE IF EXISTS  employee_proc;
DELIMITER //
CREATE PROCEDURE employee_proc()
BEGIN
  SELECT employeeId, employeeName FROM employee WHERE employeeId in (7,8);
END //
DELIMITER ;


Intent:

{
  "resultSet1": [
    {
      "employeeId": 7,
      "employeeName": "Bharth"
    },
    {
      "employeeId": 8,
      "employeeName": "Arun"
    }
  ],
  "updateCount1": 0
}

UI responseneded
[
  {
    "empid": 7,
    "empName": "Bharth"
  },
  {
    "empid": 8,
    "empName": "Arun"
  }
]